package com.jesuscalderon.climacalderon.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jesuscalderon.climacalderon.R;

import java.util.ArrayList;

public class ListadoAdapter extends ArrayAdapter<String> {

    ArrayList<String> alNomDia;
    ArrayList<String> alDia;
    ArrayList<String> alTempDia;
    ArrayList<String> alTempMax;
    ArrayList<String> alTempMin;
    ArrayList<String> alHumedad;

    Context mContext;

    public ListadoAdapter(Context context, ArrayList<String> arrNomDia, ArrayList<String> arrDia, ArrayList<String> arrTempDia, ArrayList<String> arrTempMax, ArrayList<String> arrTempMin, ArrayList<String> arrHumedad){
        super(context, R.layout.cardview_listado);

        this.alNomDia = arrNomDia;
        this.alDia = arrDia;
        this.alTempDia = arrTempDia;
        this.alTempMax = arrTempMax;
        this.alTempMin = arrTempMin;
        this.alHumedad = arrHumedad;

        this.mContext = context;
    }


    @Override
    public int getCount() {
        return alDia.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();

        if(convertView == null) {
            LayoutInflater inf = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.cardview_listado, parent, false);

            mViewHolder.textNomDia = (TextView) convertView.findViewById(R.id.tv_dia);
            mViewHolder.textDia = (TextView) convertView.findViewById(R.id.tv_date);
            mViewHolder.textTempDia = (TextView) convertView.findViewById(R.id.tv_temp);
            mViewHolder.textTempMin = (TextView) convertView.findViewById(R.id.tv_tempMin);
            mViewHolder.textTempMax = (TextView) convertView.findViewById(R.id.tv_tempMax);
            mViewHolder.textHumedad = (TextView) convertView.findViewById(R.id.tv_humedad);
            mViewHolder.btnMoreDetailDay = (ImageView) convertView.findViewById(R.id.btnMoreDetailDay);
            mViewHolder.layout = (LinearLayout) convertView.findViewById(R.id.LLcardDetails);
            mViewHolder.llMoreDetailDay = (LinearLayout) convertView.findViewById(R.id.llMoreDetailDay);

            convertView.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)convertView.getTag();
        }

        mViewHolder.textNomDia.setText(alNomDia.get(position));
        mViewHolder.textDia.setText(alDia.get(position));
        mViewHolder.textTempDia.setText(alTempDia.get(position)+"°C");
        mViewHolder.textTempMax.setText(alTempMax.get(position)+"°C");
        mViewHolder.textTempMin.setText(alTempMin.get(position)+"°C");
        mViewHolder.textHumedad.setText(alHumedad.get(position)+"%");

        final ViewHolder finalMViewHolder = mViewHolder;

        mViewHolder.llMoreDetailDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finalMViewHolder.change == 0){
                    finalMViewHolder.change = 1;
                    finalMViewHolder.layout.setVisibility(View.VISIBLE);
                    finalMViewHolder.btnMoreDetailDay.setImageResource(R.drawable.ic_uparrow);
                }else{
                    finalMViewHolder.change = 0;
                    finalMViewHolder.layout.setVisibility(View.GONE);
                    finalMViewHolder.btnMoreDetailDay.setImageResource(R.drawable.ic_downarrow);
                }
            }
        });


        return convertView;
    }

    static class ViewHolder{
        TextView textNomDia;
        TextView textDia;
        TextView textTempDia;
        TextView textTempMax;
        TextView textTempMin;
        TextView textHumedad;

        ImageView btnMoreDetailDay;
        LinearLayout layout;
        LinearLayout llMoreDetailDay;

        int change = 0;

    }
}
