package com.jesuscalderon.climacalderon.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jesuscalderon.climacalderon.Activitys.VistaDatosClimaActivity;
import com.jesuscalderon.climacalderon.R;

import java.util.ArrayList;

public class ListadoCiudadesAdapter extends ArrayAdapter<String> {

    ArrayList<String> alId;
    ArrayList<String> alNomCiudades;
    ArrayList<String> alTemp;
    Context mContext;

    public ListadoCiudadesAdapter(Context context, ArrayList<String> arrId, ArrayList<String> arrNomCiudades, ArrayList<String> arrTemp){
        super(context, R.layout.historico_listado);

        this.alId = arrId;
        this.alNomCiudades = arrNomCiudades;
        this.alTemp = arrTemp;

        this.mContext = context;
    }

    @Override
    public int getCount() {
        return alNomCiudades.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if(convertView == null) {
            LayoutInflater inf = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.historico_listado, parent, false);

            mViewHolder.tvCiudad = (TextView) convertView.findViewById(R.id.tvCiudad);
            mViewHolder.tvTemp = (TextView) convertView.findViewById(R.id.tvTemp);
            mViewHolder.llElemento = (LinearLayout) convertView.findViewById(R.id.llElemento);

            convertView.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder)convertView.getTag();
        }


        mViewHolder.tvCiudad.setText(alNomCiudades.get(position));
        mViewHolder.tvTemp.setText(alTemp.get(position));

        final ViewHolder finalMViewHolder = mViewHolder;

        mViewHolder.llElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, VistaDatosClimaActivity.class);
                intent.putExtra("id",alId.get(position));

                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder{
        TextView tvCiudad;
        TextView tvTemp;
        LinearLayout llElemento;
    }
}
