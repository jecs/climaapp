package com.jesuscalderon.climacalderon.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jesuscalderon.climacalderon.Activitys.MainActivity;

import org.json.JSONObject;


public class Requests {

    public Context context;
    public SharedPreferencesUtils sp = new SharedPreferencesUtils();
    private Response.Listener<JSONObject> listener;


    private static final String TAG = "Requests";
    private static final String APPID = "0feacb510385d235ff907121cfc8924f";
    private static final String URL = "https://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&APPID="+APPID;
    private static final String URLDIAS = "https://api.openweathermap.org/data/2.5/forecast?lang=es&units=metric&APPID="+APPID;
    private static final String URLLOCAL = "https://api.openweathermap.org/data/2.5/weather?lang=es&units=metric&APPID="+APPID;


    public Requests(Context context) {
        this.context = context;
    }


    public boolean setNetworkListener() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected) {
            Log.d(TAG, "setNetworkListener: Conectado");
            return true;
        } else {
            Log.d(TAG, "setNetworkListener: NO Conectado");
            return false;
        }
    }


    public void getClimaLatLon(double lat, double lon){

        sp.saveObjectToSharedPreference(context, "ClimaLocation", "json", "");

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format(URLLOCAL+"&lat="+lat+"&lon="+lon);

        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        sp.saveObjectToSharedPreference(context, "ClimaLocation", "json", response);
                        Log.d("getClimaLatLon","response -> "+response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;

                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch(response.statusCode){
                                case 404:
                                    json = new String(response.data);
                                    if(json != null){
                                        Log.d("RealizarPost","onErrorResponse -> "+json);
                                        sp.saveObjectToSharedPreference(context, "loginData", "json", json);
                                    }
                                    break;
                                default:
                                    Log.d(TAG,"onErrorResponse -> "+error);
                                    sp.saveObjectToSharedPreference(context, "ClimaLocation", "json", "error");
                                    break;
                            }
                        }
                    }
                }
        );
        queue.add(postRequest);
        getClimaLatLonDias(lat,lon);
    }

    public void getClimaLatLonDias(double lat, double lon){

        sp.saveObjectToSharedPreference(context, "ClimaLocationDias", "json", "");

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format(URLDIAS+"&lat="+lat+"&lon="+lon);

        Log.d(TAG,url);
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("getClimaLatLonDias","response -> "+response);
                        sp.saveObjectToSharedPreference(context, "ClimaLocationDias", "json", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;

                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch(response.statusCode){
                                case 404:
                                    json = new String(response.data);
                                    if(json != null){
                                        Log.d("RealizarPost","onErrorResponse -> "+json);
                                        sp.saveObjectToSharedPreference(context, "loginData", "json", json);
                                    }
                                    break;
                                default:
                                    Log.d(TAG,"onErrorResponse -> "+error);
                                    sp.saveObjectToSharedPreference(context, "ClimaLocationDias", "json", "error");
                                    break;
                            }
                        }
                    }
                }
        );
        queue.add(postRequest);
    }

    public void getClima(String sCiudad){
        sp.saveObjectToSharedPreference(context, "ClimaLocation", "json", "");
        sp.saveObjectToSharedPreference(context, "ClimaLocation", "resp", false);
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Request.Method.GET, URL+"&q="+sCiudad,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG,"onResponse -> "+response);
                        sp.saveObjectToSharedPreference(context, "ClimaLocation", "json", response);
                        sp.saveObjectToSharedPreference(context, "ClimaLocation", "resp", true);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;

                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch(response.statusCode){
                                case 404:
                                    json = new String(response.data);
                                    if(json != null){
                                        Log.d(TAG,"onErrorResponse -> "+json);
                                        sp.saveObjectToSharedPreference(context, "ClimaLocation", "json", "error");
                                        sp.saveObjectToSharedPreference(context, "ClimaLocation", "resp", true);
                                    }
                                    break;
                                default:
                                    Log.d(TAG,"onErrorResponse -> "+error);
                                    break;
                            }
                        }
                    }
                }
        );
        queue.add(postRequest);

        getClimaDias(sCiudad);
    }

    public void getClimaDias(String sCiudad){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest postRequest = new StringRequest(Request.Method.GET, URLDIAS+"&q="+sCiudad,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG,"onResponse -> "+response);
                        sp.saveObjectToSharedPreference(context, "ClimaLocationDias", "json", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String json = null;

                        NetworkResponse response = error.networkResponse;
                        if(response != null && response.data != null){
                            switch(response.statusCode){
                                case 404:
                                    json = new String(response.data);
                                    if(json != null){
                                        Log.d(TAG,"onErrorResponse -> "+json);
                                    }
                                    break;
                                default:
                                    Log.d(TAG,"onErrorResponse -> "+error);
                                    break;
                            }
                        }
                    }
                }
        );
        queue.add(postRequest);
    }


}
