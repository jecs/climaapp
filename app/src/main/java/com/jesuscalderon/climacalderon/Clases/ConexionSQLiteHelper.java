package com.jesuscalderon.climacalderon.Clases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ConexionSQLiteHelper extends SQLiteOpenHelper {

    public ConexionSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE ubicaciones(id INTEGER PRIMARY KEY, nombre TEXT, temperatura TEXT, temminima TEXT, temmaxima TEXT, humedad TEXT, idimagen TEXT, jsonclimadias TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAntigua, int versionNueva) {
        db.execSQL("DROP TABLE IF EXISTS ubicaciones;");

        onCreate(db);
    }
}
