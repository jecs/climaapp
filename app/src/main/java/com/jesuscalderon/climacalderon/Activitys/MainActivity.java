package com.jesuscalderon.climacalderon.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jesuscalderon.climacalderon.Adapter.ListadoAdapter;
import com.jesuscalderon.climacalderon.Clases.ConexionSQLiteHelper;
import com.jesuscalderon.climacalderon.R;
import com.jesuscalderon.climacalderon.utils.Requests;
import com.jesuscalderon.climacalderon.utils.SharedPreferencesUtils;
import com.jesuscalderon.climacalderon.utils.TimeOut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public SharedPreferencesUtils sp = new SharedPreferencesUtils();
    public String grados = "°C";
    public String sCiudadABuscar= "";
    public String JsonClima;
    public String JsonDiasClima;
    public String sIdImagen;
    public String sJsonClimaAux;

    TextView tv_Titulo;
    TextView tv_ClimaHoy;
    TextView tv_tempMin;
    TextView tv_tempMax;
    TextView tv_humedad;
    ImageView imgClima;
    ImageView btnBusqueda;
    ImageView btnMoreLoc;

    ArrayList<String> alNomDia = new ArrayList<>();
    ArrayList<String> alDia = new ArrayList<>();
    ArrayList<String> alTempDia = new ArrayList<>();
    ArrayList<String> alTempMax = new ArrayList<>();
    ArrayList<String> alTempMin = new ArrayList<>();
    ArrayList<String> alHumedad = new ArrayList<>();

    ListadoAdapter ListadoAdapter;
    Requests requests;
    ListView mListView;
    ProgressDialog progressDialog;
    ConexionSQLiteHelper conn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requests = new Requests(this);

        progressDialog = new ProgressDialog(this);

        conn = new ConexionSQLiteHelper(this,"bd_ubicaciones",null,1);

        tv_Titulo = findViewById(R.id.tv_Titulo);
        tv_ClimaHoy = findViewById(R.id.tv_ClimaHoy);
        tv_tempMin = findViewById(R.id.tv_tempMin);
        tv_tempMax = findViewById(R.id.tv_tempMax);
        tv_humedad = findViewById(R.id.tv_humedad);
        mListView = (ListView) findViewById(R.id.lv_listado);
        imgClima = findViewById(R.id.imgClima);
        btnBusqueda = findViewById(R.id.btnBuscarLoc);
        btnMoreLoc = findViewById(R.id.btnMoreLoc);
        btnBusqueda.setOnClickListener(this);
        btnMoreLoc.setOnClickListener(this);



        validacionInical();
    }

    public void validacionInical(){
        sJsonClimaAux = sp.getSavedObjectFromPreference(this, "ClimaLocation", "json", String.class);

        if(!requests.setNetworkListener()) {
            if (!consultarDatos()) {
                tv_Titulo.setText("--");
                tv_ClimaHoy.setText("--" + grados);
                tv_tempMin.setText(getResources().getString(R.string.sLayoutTempMin) + "--" + grados);
                tv_tempMax.setText(getResources().getString(R.string.sLayoutTempMax) + "--" + grados);
                tv_humedad.setText(getResources().getString(R.string.sLayoutHumedad) + "--" + "%");

                Toast.makeText(getBaseContext(),"No cuentas con Internet",Toast.LENGTH_LONG).show();

                detectarInternet();
            }
        }else{
            if(sJsonClimaAux.equals("")){
                TimeOut.delay(2, () -> validacionInical());
            }else{
                JsonClima = sp.getSavedObjectFromPreference(this, "ClimaLocation", "json", String.class);
                JsonDiasClima = sp.getSavedObjectFromPreference(this, "ClimaLocationDias", "json", String.class);

                if(!JsonDiasClima.equals("") && !JsonClima.equals("")){
                    visualizarDatos();
                }else{

                }
            }
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnBuscarLoc:
                mostrarDialogo();
                break;
            case R.id.btnMoreLoc:
                startActivity(new Intent(this,listadoCiudadesActivity.class));
                break;
            default:

                break;
        }
    }

    public void busquedaCiudad(){
        progressDialog = ProgressDialog.show(this,"","Cargando...",true,false);
        if(sCiudadABuscar.equals("")){
            progressDialog.dismiss();
            mostrarDialogo();

        }else{

            requests.getClima(sCiudadABuscar);
            TimeOut.delay(2, () -> {
                llenadoPorBusqueda();
            });

        }
    }

    public void llenadoPorBusqueda(){
        Boolean bAccedioDats = sp.getSavedObjectFromPreference(this, "ClimaLocation", "resp", Boolean.class);

        if(bAccedioDats){
            JsonClima = sp.getSavedObjectFromPreference(this, "ClimaLocation", "json", String.class);
            JsonDiasClima = sp.getSavedObjectFromPreference(this, "ClimaLocationDias", "json", String.class);
            if(JsonClima.equals("error")){
                progressDialog.dismiss();

                Toast.makeText(getBaseContext(),"No se encuentra la ciudad que seleccionaste",Toast.LENGTH_LONG).show();

            }else{
                visualizarDatos();
                progressDialog.dismiss();
            }


        }else{
            TimeOut.delay(2, () -> llenadoPorBusqueda());
        }
    }

    public void visualizarDatos(){
        llenadoDeDatosHoy();
        llenadoListado();
        if(!verificarDatosBD()){
            registrarUbicacion();
        }
    }

    public void mostrarDialogo(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.busqueda_ubicacion);

        ImageView btnCancelar = dialog.findViewById(R.id.btnCancelar);
        TextView btnAceptar = dialog.findViewById(R.id.btnAceptar);
        EditText etBusqueda = dialog.findViewById(R.id.etBusqueda);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sCiudadABuscar="";
                sCiudadABuscar = etBusqueda.getText().toString();

                if(requests.setNetworkListener()){
                    busquedaCiudad();
                }else{
                    Toast.makeText(getBaseContext(),"No cuentas con Internet",Toast.LENGTH_LONG).show();
                }

                dialog.dismiss();

            }
        });
        dialog.show();

    }

    public void detectarInternet(){

        if(requests.setNetworkListener()){
            requests.getClimaLatLon(24.8,-107.39); //defecto Culiacan
            TimeOut.delay(10,()->{

                JsonClima = sp.getSavedObjectFromPreference(this, "ClimaLocation", "json", String.class);
                JsonDiasClima = sp.getSavedObjectFromPreference(this, "ClimaLocationDias", "json", String.class);

                if(!JsonDiasClima.equals("") && !JsonClima.equals("")){
                    visualizarDatos();
                }
                    });

        }else{
            TimeOut.delay(20, () -> detectarInternet());
        }
    }

    public String jsonToValue(JSONObject jsonObject, String campo) {

        String regreso = "";
        if (jsonObject.has(campo)) {
            try {
                regreso = jsonObject.getString(campo);
                if(regreso.equals(null) || regreso.equals("null")){
                    regreso = "";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return regreso;
    }

    public void llenadoDeDatosHoy(){

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        try{
            JSONObject jsonObjeto = new JSONObject(JsonClima);

            if (jsonObjeto.has("main")) {
                JSONObject jsonMain = new JSONObject(jsonToValue(jsonObjeto,"main"));

                tv_Titulo.setText(jsonToValue(jsonObjeto,"name").toUpperCase());

                tv_ClimaHoy.setText(jsonToValue(jsonMain,"temp").substring(0,2)+grados);

                tv_tempMin.setText(getResources().getString(R.string.sLayoutTempMin)+ jsonToValue(jsonMain,"temp_min").substring(0,2) +grados);
                tv_tempMax.setText(getResources().getString(R.string.sLayoutTempMax)+ jsonToValue(jsonMain,"temp_max").substring(0,2) +grados);
                tv_humedad.setText(getResources().getString(R.string.sLayoutHumedad)+ jsonToValue(jsonMain,"humidity").substring(0,2)+"%");

            }else{
                Log.d("MAIN","==== ELSE jsonObjeto -> main ====");
            }

            if(jsonObjeto.has("weather")){

                JSONArray jsonWeather = new JSONArray(jsonToValue(jsonObjeto,"weather"));
                String sJsonW = jsonWeather.getString(0);

                JSONObject jsonW = new JSONObject(sJsonW);
                sIdImagen = jsonToValue(jsonW,"id");
                switch (sIdImagen){
                    case "200"://Thunderstorm
                        break;
                    case "500"://Rain
                        imgClima.setImageResource(R.drawable.ic_rain);
                        break;
                    case "600"://Snow
                        imgClima.setImageResource(R.drawable.ic_winter);
                        break;
                    case "800"://Clear
                        if(Integer.parseInt(currentTime.substring(0,2))>18){
                            imgClima.setImageResource(R.drawable.ic_moon);
                        }else{
                            imgClima.setImageResource(R.drawable.ic_sun);
                        }
                        break;
                    case"801"://Clouds
                    case"802":
                    case"803":
                    case"804":
                        if(Integer.parseInt(currentTime.substring(0,2))>18){
                            imgClima.setImageResource(R.drawable.ic_night_cloud);
                        }else{
                            imgClima.setImageResource(R.drawable.ic_cloudy);
                        }

                        break;
                    default:
                        break;
                }
            }

        }catch(Exception ex){
            Log.d("MAIN","==== Exception ==== "+ex);
        }
    }

    public void llenadoListado(){

        lecturaJson();

        try{
            ListadoAdapter = new ListadoAdapter(this,alNomDia,alDia,alTempDia,alTempMax,alTempMin,alHumedad);
            ListadoAdapter.clear();
            ListadoAdapter.notifyDataSetChanged();


            mListView.setAdapter(ListadoAdapter);
        }catch (Exception ex){
            Log.d("MAIN","llenadoListado - Exception "+ex);
        }

    }

    public void lecturaJson(){
        int dia = 0;
        int diaSemana = 1;

        alDia.clear();
        alTempDia.clear();
        alTempMax.clear();
        alTempMin.clear();
        alHumedad.clear();

        alNomDia.clear();

        try{
            JSONObject jsonDiasClima = new JSONObject(JsonDiasClima);
            if (jsonDiasClima.has("list")) {

                JSONArray jsonListWeather = new JSONArray(jsonToValue(jsonDiasClima,"list"));
                for(int i = 0; i< jsonListWeather.length();i++){
                    JSONObject jsonAux = new JSONObject(jsonListWeather.getString(i));

                    if (jsonAux.has("main")) {
                        JSONObject jsonDiasMain = new JSONObject(jsonToValue(jsonAux,"main"));
                        if(dia < Integer.parseInt(jsonToValue(jsonAux,"dt_txt").substring(8,10))){
                            dia = Integer.parseInt(jsonToValue(jsonAux,"dt_txt").substring(8,10));

                            alDia.add(jsonToValue(jsonAux,"dt_txt").substring(0,10));
                            alTempDia.add(jsonToValue(jsonDiasMain,"temp").substring(0,2));
                            alTempMax.add(jsonToValue(jsonDiasMain,"temp_max").substring(0,2));
                            alTempMin.add(jsonToValue(jsonDiasMain,"temp_min").substring(0,2));
                            alHumedad.add(jsonToValue(jsonDiasMain,"humidity"));

                            alNomDia.add(dia+"-"+jsonToValue(jsonAux,"dt_txt").substring(5,7)+"-"+jsonToValue(jsonAux,"dt_txt").substring(0,4));
                            diaSemana++;
                        }

                    }else{
                        Log.d("MAIN","==== ELSE jsonAux -> main====");
                    }

                }



            }else{

            }

        }catch (Exception ex){

        }


    }

    public Boolean verificarDatosBD(){
        Boolean bResp = false;
        SQLiteDatabase db=conn.getReadableDatabase();

        String[] condicion={ tv_Titulo.getText().toString()};

        try {
            Cursor cursor=db.rawQuery("SELECT nombre FROM ubicaciones WHERE nombre=?",condicion);

            while (cursor.moveToNext()){
                Log.d("verificarDatosBD",cursor.getString(0));
                bResp=true;
            }


        }catch (Exception ex){
            Log.d("MAIN","consultarDatos Exception -> "+ex);
        }

        return bResp;
    }

    public Boolean consultarDatos(){
        Boolean bResp = false;
        SQLiteDatabase db=conn.getReadableDatabase();

        try {
            Cursor cursor=db.rawQuery("SELECT nombre,temperatura,temminima,temmaxima,humedad,idimagen,jsonclimadias FROM ubicaciones ORDER BY id DESC LIMIT 1;",null);

            cursor.moveToFirst();

            if(cursor.getString(0).length()>0){
                tv_Titulo.setText(cursor.getString(0));
                tv_ClimaHoy.setText(cursor.getString(1));
                tv_tempMin.setText(cursor.getString(2));
                tv_tempMax.setText(cursor.getString(3));
                tv_humedad.setText(cursor.getString(4));
                sIdImagen=cursor.getString(5);
                JsonDiasClima=cursor.getString(6);
                llenadoListado();
                bResp=true;
            }

        }catch (Exception ex){
            Log.d("MAIN","consultarDatos Exception -> "+ex);
        }

        return bResp;
    }

    public void registrarUbicacion(){

        SQLiteDatabase db=conn.getWritableDatabase();

        if(!tv_Titulo.getText().toString().equals("--")){

            ContentValues values = new ContentValues();
            values.put("nombre",tv_Titulo.getText().toString());
            values.put("temperatura",tv_ClimaHoy.getText().toString());
            values.put("temminima",tv_tempMin.getText().toString());
            values.put("temmaxima",tv_tempMax.getText().toString());
            values.put("humedad",tv_humedad.getText().toString());
            values.put("idimagen",sIdImagen);
            values.put("jsonclimadias",JsonDiasClima);

            Long idResultante=db.insert("ubicaciones","id",values);

            //Toast.makeText(getApplicationContext(),"Id Registro: "+idResultante,Toast.LENGTH_SHORT).show();
            Log.d("MAIN","Id Registro: "+idResultante);
            db.close();
        }
    }
}
