package com.jesuscalderon.climacalderon.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jesuscalderon.climacalderon.Adapter.ListadoAdapter;
import com.jesuscalderon.climacalderon.Clases.ConexionSQLiteHelper;
import com.jesuscalderon.climacalderon.R;
import com.jesuscalderon.climacalderon.utils.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class VistaDatosClimaActivity extends AppCompatActivity implements View.OnClickListener {

    ConexionSQLiteHelper conn;

    public String sId;
    public String sIdImagen;
    public String JsonDiasClima;

    TextView tv_Titulo;
    TextView tv_ClimaHoy;
    TextView tv_tempMin;
    TextView tv_tempMax;
    TextView tv_humedad;
    ImageView imgClima;
    ImageView btnBack;

    ArrayList<String> alNomDia = new ArrayList<>();
    ArrayList<String> alDia = new ArrayList<>();
    ArrayList<String> alTempDia = new ArrayList<>();
    ArrayList<String> alTempMax = new ArrayList<>();
    ArrayList<String> alTempMin = new ArrayList<>();
    ArrayList<String> alHumedad = new ArrayList<>();

    ListadoAdapter ListadoAdapter;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vistadatosclima);
        Intent intent = getIntent();

        sId = intent.getStringExtra("id");

        conn = new ConexionSQLiteHelper(this,"bd_ubicaciones",null,1);

        tv_Titulo = findViewById(R.id.tv_Titulo);
        tv_ClimaHoy = findViewById(R.id.tv_ClimaHoy);
        tv_tempMin = findViewById(R.id.tv_tempMin);
        tv_tempMax = findViewById(R.id.tv_tempMax);
        tv_humedad = findViewById(R.id.tv_humedad);
        mListView = (ListView) findViewById(R.id.lv_listado);
        imgClima = findViewById(R.id.imgClima);
        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        verificarDatosBD();
    }


    public Boolean verificarDatosBD(){
        Boolean bResp = false;
        SQLiteDatabase db=conn.getReadableDatabase();

        String[] condicion={ sId };

        try {
            Cursor cursor=db.rawQuery("SELECT nombre,temperatura,temminima,temmaxima,humedad,idimagen,jsonclimadias FROM ubicaciones WHERE id = ? ORDER BY id DESC LIMIT 1;",condicion);

            cursor.moveToFirst();

            if(cursor.getString(0).length()>0){
                tv_Titulo.setText(cursor.getString(0));
                tv_ClimaHoy.setText(cursor.getString(1));
                tv_tempMin.setText(cursor.getString(2));
                tv_tempMax.setText(cursor.getString(3));
                tv_humedad.setText(cursor.getString(4));
                sIdImagen=cursor.getString(5);
                JsonDiasClima=cursor.getString(6);
                llenadoListado();
                bResp=true;
            }


        }catch (Exception ex){
            Log.d("MAIN","consultarDatos Exception -> "+ex);
        }

        return bResp;
    }

    public void cambiarImagen(String sIdImagen){

        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        switch (sIdImagen){
            case "200"://Thunderstorm
                break;
            case "500"://Rain
                imgClima.setImageResource(R.drawable.ic_rain);
                break;
            case "600"://Snow
                imgClima.setImageResource(R.drawable.ic_winter);
                break;
            case "800"://Clear
                if(Integer.parseInt(currentTime.substring(0,2))>18){
                    imgClima.setImageResource(R.drawable.ic_moon);
                }else{
                    imgClima.setImageResource(R.drawable.ic_sun);
                }
                break;
            case"801"://Clouds
            case"802":
            case"803":
            case"804":
                if(Integer.parseInt(currentTime.substring(0,2))>18){
                    imgClima.setImageResource(R.drawable.ic_night_cloud);
                }else{
                    imgClima.setImageResource(R.drawable.ic_cloudy);
                }

                break;
            default:
                break;
        }
    }

    public void llenadoListado(){
        lecturaJson();

        try{
            ListadoAdapter = new ListadoAdapter(this,alNomDia,alDia,alTempDia,alTempMax,alTempMin,alHumedad);
            ListadoAdapter.clear();
            mListView.setAdapter(ListadoAdapter);
        }catch (Exception ex){
            Log.d("MAIN","llenadoListado - Exception "+ex);
        }

    }

    public void lecturaJson(){
        int dia = 0;
        int diaSemana = 1;

        try{
            JSONObject jsonDiasClima = new JSONObject(JsonDiasClima);
            if (jsonDiasClima.has("list")) {

                JSONArray jsonListWeather = new JSONArray(jsonToValue(jsonDiasClima,"list"));
                for(int i = 0; i< jsonListWeather.length();i++){
                    JSONObject jsonAux = new JSONObject(jsonListWeather.getString(i));

                    if (jsonAux.has("main")) {
                        JSONObject jsonDiasMain = new JSONObject(jsonToValue(jsonAux,"main"));
                        if(dia < Integer.parseInt(jsonToValue(jsonAux,"dt_txt").substring(8,10))){
                            dia = Integer.parseInt(jsonToValue(jsonAux,"dt_txt").substring(8,10));

                            alDia.add(jsonToValue(jsonAux,"dt_txt").substring(0,10));
                            alTempDia.add(jsonToValue(jsonDiasMain,"temp").substring(0,2));
                            alTempMax.add(jsonToValue(jsonDiasMain,"temp_max").substring(0,2));
                            alTempMin.add(jsonToValue(jsonDiasMain,"temp_min").substring(0,2));
                            alHumedad.add(jsonToValue(jsonDiasMain,"humidity"));

                            alNomDia.add(dia+"-"+jsonToValue(jsonAux,"dt_txt").substring(5,7)+"-"+jsonToValue(jsonAux,"dt_txt").substring(0,4));
                            diaSemana++;
                        }

                    }else{
                        Log.d("MAIN","==== ELSE jsonAux -> main====");
                    }

                }

            }else{

            }

        }catch (Exception ex){

        }


    }


    public String jsonToValue(JSONObject jsonObject, String campo) {

        String regreso = "";
        if (jsonObject.has(campo)) {
            try {
                regreso = jsonObject.getString(campo);
                if(regreso.equals(null) || regreso.equals("null")){
                    regreso = "";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return regreso;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnBack:
                finish();
                break;
        }
    }
}
