package com.jesuscalderon.climacalderon.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.jesuscalderon.climacalderon.Adapter.ListadoCiudadesAdapter;
import com.jesuscalderon.climacalderon.Clases.ConexionSQLiteHelper;
import com.jesuscalderon.climacalderon.R;

import java.util.ArrayList;

public class listadoCiudadesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView btnToolbarAtras;

    ArrayList<String> alId = new ArrayList<>();
    ArrayList<String> alNombreCiudad = new ArrayList<>();
    ArrayList<String> alTemp = new ArrayList<>();

    ListadoCiudadesAdapter ListadoCiudadesAdapter;
    ListView mListado;

    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_ciudades);

        conn = new ConexionSQLiteHelper(this,"bd_ubicaciones",null,1);

        btnToolbarAtras = findViewById(R.id.btnToolbarAtras);
        btnToolbarAtras.setOnClickListener(this);
        mListado = (ListView) findViewById(R.id.lv_listado);

        llenadoDeDatos();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnToolbarAtras:
                finish();
                break;
        }
    }

    public void llenadoDeDatos(){

        verificarDatosBD();


        try{
            ListadoCiudadesAdapter = new ListadoCiudadesAdapter(this, alId, alNombreCiudad, alTemp);
            ListadoCiudadesAdapter.clear();
            mListado.setAdapter(ListadoCiudadesAdapter);
        }catch (Exception ex){

        }

    }

    public void verificarDatosBD(){
        SQLiteDatabase db=conn.getReadableDatabase();


        try {
            Cursor cursor=db.rawQuery("SELECT id, nombre, temperatura FROM ubicaciones order by id",null);

            while (cursor.moveToNext()){
                Log.d("verificarDatosBD",cursor.getString(0));
                alId.add(cursor.getString(0));
                alNombreCiudad.add(cursor.getString(1));
                alTemp.add(cursor.getString(2));
            }

        }catch (Exception ex){
            Log.d("MAIN","consultarDatos Exception -> "+ex);
        }

    }
}
