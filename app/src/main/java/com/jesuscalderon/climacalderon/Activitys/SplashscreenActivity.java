package com.jesuscalderon.climacalderon.Activitys;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.util.Log;

import com.jesuscalderon.climacalderon.Clases.ConexionSQLiteHelper;
import com.jesuscalderon.climacalderon.R;
import com.jesuscalderon.climacalderon.utils.Requests;
import com.jesuscalderon.climacalderon.utils.SharedPreferencesUtils;
import com.jesuscalderon.climacalderon.utils.TimeOut;

import java.util.List;
import java.util.Locale;

public class SplashscreenActivity extends AppCompatActivity {

    public SharedPreferencesUtils sp = new SharedPreferencesUtils();

    private static final int PERMISO_UBICACION = 0;
    public String sUbicacion = "";

    Requests requests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        ConexionSQLiteHelper conn = new ConexionSQLiteHelper(this,"bd_ubicaciones",null,1);


        requests = new Requests(this);
        sUbicacion = sp.getSavedObjectFromPreference(this, "dataHistory", "ubicacion", String.class);

        if (requests.setNetworkListener()) {
            validarPermisos();
        }else{
            // Mensaje no cuentas con internet
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, PERMISO_UBICACION);
            iniciarActividad();
        }

    }

    public Boolean obtenerUbicacion() {
        Boolean bRespUb = false;

        LocationManager lm = (LocationManager) getSystemService(this.LOCATION_SERVICE);
        try {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            double longitude = location.getLongitude();
            double latitude = location.getLatitude();

            Log.d("SplashScreen","latitude-> "+latitude+"    longitude->"+longitude);

            requests.getClimaLatLon(latitude,longitude);

            bRespUb = true;

        }catch (Exception ex){
            requests.getClimaLatLon(24.8,-107.39); //defecto Culiacan
            Log.d("SplashScreen","Exception-> "+ex);
        }

        return bRespUb;
    }
    public void iniciarActividad(){
        new android.os.Handler().postDelayed(
            new Runnable() {
                public void run() {
                    startActivity(new Intent(SplashscreenActivity.this,MainActivity.class));
                    finish();
                }
            },
            3220);

    }

    public void validarPermisos(){
        if(canceloPermisoUbicacion()){
            informacion();
            sp.saveObjectToSharedPreference(this, "ClimaLocation", "EnableUbicacion", false);
        }
        if(!permisoUbicacion()){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION}, PERMISO_UBICACION);
            TimeOut.delay(2, () -> validarPermisos());
        }else{
            if(obtenerUbicacion()){
                iniciarActividad();
            }else{
                TimeOut.delay(2,() ->iniciarActividad());

            }
        }
    }

    public void informacion(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Ubicación").setMessage("Elegiste no usar la ubicación.\nSolo te mostraremos los lugares que tu selecciones")
                .setPositiveButton("Aceptar",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        iniciarActividad();
                    }
                });
        dialog.show();
    }


    private Boolean permisoUbicacion() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
    private Boolean canceloPermisoUbicacion(){
        return ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION);
    }
}
